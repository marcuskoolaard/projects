# Author of this file:
# Marcus Koolaard
#
# Sudoku solver that builds a recursive backtracking solution
# that branches on possible values that could be placed in the next empty cell.
# Initial pruning of the recursion tree - 
# -> Don't continue on any branch that has already produced an inconsistent solution
# -> Stop and return a complete solution once one has been found

import pygame, Sudoku_IO

def solve(snapshot, screen):
    # display current snapshot
    pygame.time.delay(200)
    Sudoku_IO.displayPuzzle(snapshot, screen)
    pygame.display.flip()

    listOfUnsolved = snapshot.findPossVals()
    # listOfUnsolved holds cells sorted by number of possible values,
    # so when iterated over, will choose singletons FIRST (lowest number of possible values)

    if checkConsistency(snapshot):
        if isComplete(snapshot):
            print("COMPLETE")
            return True     # base case, board is consistent, AND is complete: must be finished so return true

        clone = snapshot.clone()    # create clone

        for x in (listOfUnsolved[0]).possibleValues:    # will pick the cell with the least possible values (usually one, i.e. a singleton, never encounted more than 2)
            clone.setCellVal(listOfUnsolved[0].getRow(), listOfUnsolved[0].getCol(), x)     # try that value in that spot and continue
            if solve(clone, screen):        # recursive call
                return True

        else:
            return False

    # if current snapshot is complete ... return a value
    # if current snapshot not complete ...
    # for each possible value for an empty cell
    # clone current snapshot and update it,
    # if new snapshot is consistent, perform recursive call
    # return a value
    

# Check whether a snapshot is consistent, i.e. all cell values comply 
# with the sudoku rules (each number occurs only once in each block, row and column). 
     
def checkConsistency(snapshot):
    for i in range(9):
        rows = snapshot.cellsByRow(i)
        valuesR=[]
        for row in rows:
            valuesR.append(row.getVal())
        for n in range(1,10):
            if valuesR.count(n) > 1:
                return False

        cols = snapshot.cellsByCol(i)
        valuesC=[]
        for col in cols:
            valuesC.append(col.getVal())
        for n in range(1, 10):
            if valuesC.count(n) > 1:
                return False

        for j in range(9):
            blocks = snapshot.cellsByBlock(i,j)
            valuesB = []
            for block in blocks:
                valuesB.append(block.getVal())
            for n in range(1, 10):
                if valuesB.count(n) > 1:
                    return False
    return True

# Check whether a puzzle is solved. 
# return true if the sudoku is solved, false otherwise
     
def isComplete(snapshot):
    if len(snapshot.unsolvedCells()) == 0:
        return True

