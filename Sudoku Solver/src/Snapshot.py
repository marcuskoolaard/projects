# A snapshot is a point in the computation when the values for some, but possibly not all, cells are known.
# This class has some methods that allow to clone a snapshot (this is useful for producing the next snapshots in the recursion tree)
# to query the cells in various ways, and to set cell values.

import Cell

class snapshot:
    def __init__(self):
        self.rows = 9
        self.columns = 9
        self.cells = []
        for row in range(9):
            # Add an empty array that will hold each cell in this row
            self.cells.append([])
            for column in range(9):
                self.cells[row].append(Cell.cell(row, column, 0)) # Append a cell
                
    def setCellVal(self, i, j, val):
        self.cells[i][j].setVal(val)
        
    def getCellVal(self, i,j):
        return self.cells[i][j].getVal()
    
    def cellsByRow(self,row):
        return self.cells[row]
    
    def cellsByCol(self,col):
        column = []
        for row in range(9):
            column.append(self.cells[row][col])
        return column
    
    def cellsByBlock(self, row, col):
        block = []
        for i in range(9):
            for j in range(9):     
              if (i//3 == row//3) and (j//3 == col//3):
                 block.append(self.cells[i][j])
        return block
    
    def unsolvedCells(self):
        unsolved = []
        for row in range(9):
            for col in range(9):
              if self.cells[row][col].getVal() == 0 :
                 unsolved.append(self.cells[row][col])
        # unsolved.sort(key = lambda cell: len(cell.possibleValues))
        return unsolved
        
    def clone(self):
        clone = snapshot()
        for row in range(9):
            for col in range(9):
              clone.setCellVal(row,col,self.getCellVal(row,col))
        return clone

    def findPossVals(self):     # as name suggests, finds the possible values for each cell, then sorts with the cell with least number of possible values first. Minimizing mistakes in picking values for each cell.
        listofCellsUnsolved = self.unsolvedCells()
        for cell in listofCellsUnsolved:
            cell.possibleValues = [1,2,3,4,5,6,7,8,9]   # list that holds the possible values for each cell, initially set to all values.
            rowOfCell = cell.getRow()       # int
            columnOfCell = cell.getCol()    # int

            cellsByRow = snapshot.cellsByRow(self, rowOfCell)
            for cell2 in cellsByRow:
                value = cell2.getVal()
                # print("ROW",value)
                if value in cell.possibleValues:
                    (cell.possibleValues).remove(value)     # removing values from that possibleValue list that will make the board inconsistent

            cellsByCol = snapshot.cellsByCol(self, columnOfCell)
            for cell3 in cellsByCol:
                value = cell3.getVal()
                if value in cell.possibleValues:
                    (cell.possibleValues).remove(value)     # removing values from that possibleValue list that will make the board inconsistent

            cellsByBlock = snapshot.cellsByBlock(self, rowOfCell, columnOfCell)
            for cell4 in cellsByBlock:
                value = cell4.getVal()
                if value in cell.possibleValues:
                    (cell.possibleValues).remove(value)  # removing values from that possibleValue list that will make the board inconsistent
        listofCellsUnsolved.sort(key = lambda cell: len(cell.possibleValues))
        # IMPORTANT: sorts listofCellsUnsolved by the length of their possible values list.
        # In other words, singletons are FIRST in the list, and otherwise the cells with the LEAST
        # number of possible cells, making the algorithm as efficient as possible in picking values
        # for each unsolved cell.
        return listofCellsUnsolved