import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Comparator;

class IRIS_OpenMP {


	public static void main(String[] args) {
		
		String traininginstances[] = new String[135];
		String testinstances[] = new String[15];
		
		openFile("traininginstances.txt", traininginstances);
		openFile("testinstances.txt", testinstances);
		
		String classes[] = {"Iris-setosa","Iris-versicolor","Iris-virginica"}; 
		long entirestart = System.nanoTime();
		
		// omp parallel for
		for (int k = 1; k < 101; k++){
			
			long start = System.nanoTime();
			int result[] = KNNAlgorithm(testinstances ,traininginstances, classes , k); 
			long end = System.nanoTime();
			double elapse = (end-start)*0.000001;
			System.out.println("Elasped time of the KNN Algorithm in milliseconds with k=" + k + " is: " + elapse);
			
			int correct = 0;
			for (int i=0;i<result.length;i++)
				correct = correct + result[i];
			System.out.println("The algorithm correctly classified " + correct + "/15 test instances with k="+k + "\n");
		}
		
		long totalend = System.nanoTime();
		double totalelapse = (totalend-entirestart)*0.000001;
		System.out.println("Total elasped time of the KNN Algorithm run in range(k=1,k=101) in milliseconds: " + totalelapse);
		
	}

	private static int[] KNNAlgorithm(String[] testingset, String[] trainingset, String[] classes, int k) {
		
		int trainsize = trainingset.length;
		int correct[] = new int[testingset.length];
		
		// omp parallel for
		for (int i=0;i<testingset.length;i++) {	// loop over testing set
	
			NearestInstance distancesANDpositions[] = new NearestInstance[trainsize]; //holds the distances between test instance yi and all training instances
			
			for (int j=0;j<trainsize;j++) {
				// calculate the distances of each training instance, this is 'embarassingly parallel',
				// but overhead too huge to run in parallel for given input
				double dist = calculateDistance(testingset[i], trainingset[j]);
				distancesANDpositions[j] = new NearestInstance(j, dist); // use custom object so that even when sorting, positions will be kept.
				
			}
			
			//get smallest k distances (and corresponding training instances) into a priority queue (min heap)
			PriorityQueue<NearestInstance> queue = NearestInstance.sortByDistance(distancesANDpositions, k); 
			
			// Output majority class of the k neighbours
			String classifiedclassofy = findMajority(queue, trainingset, classes);
			String actualclassofy = testingset[i].split(",")[4];
			
			if (classifiedclassofy.equals(actualclassofy)){
				correct[i] = 1; // using an array of zeros and ones instead of a counting integer removes race condition
			} else {
				correct[i] =0;
			}
		}

		return correct;
	}

	private static String findMajority(PriorityQueue<NearestInstance> KNNqueue, String[] trainingset, String[] classes) {
		
		int n = KNNqueue.size();
		
		// Insert all elements in hashmap where key is the class (index), and value is the frequency of the class in the queue of K nearest neighbors
        Map<Integer, Integer> hp = new HashMap<Integer, Integer>();
         
        for(int i = 0; i < n; i++)
        {
            int index = KNNqueue.remove().Position;//grab the index of the ith neighbor in the queue so we can get their IRIS class from the trainingset
            String ithneighborsclass = trainingset[index].split(",")[4];
            
			// The key is either 1,2 or 3 depending on the class of the ith neighbor, and we use this value as the key in the hashmap
            int key = Arrays.asList(classes).indexOf(ithneighborsclass); 
            
            if(hp.containsKey(key))
            {
                int freq = hp.get(key);
                freq++;
                hp.put(key, freq);
            }
            else
            {
                hp.put(key, 1);
            }
        }
         
		 // find max frequency.
        int max_count = 0, res = -1;
         
        for(Entry<Integer, Integer> val : hp.entrySet())
        {
            if (max_count < val.getValue())
            {
                res = val.getKey();
                max_count = val.getValue();
            }
        }
        
		return classes[res];
	}
	
	// Function takes a test vector y and a training vector x and returns the euclidean distance (d) between x and y.
	// Note the vectors are passed in as strings, they must be split first.
	private static double calculateDistance(String ystr, String xstr) {

		ArrayList<Double> y = new ArrayList<Double>();
		ArrayList<Double> x = new ArrayList<Double>();
		
		String[] ylist = ystr.split(",");
		String[] xlist = xstr.split(",");
		
		for (int i=0;i<4;i++) {	
			y.add(Double.parseDouble(ylist[i]));
			x.add(Double.parseDouble(xlist[i]));		
		}
		
		double d = 0;
		
		for (int i=0;i<4;i++) {
			d += Math.pow((y.get(i)-x.get(i)),2);	
		}
		
		d = Math.sqrt(d);
		
		return d;
	}
	
	private static void openFile(String filename, String[] list) {

		File file = new File(filename);
		BufferedReader reader = null;

		try {
		    reader = new BufferedReader(new FileReader(file));
		    String text = null;
			
		    int i = 0;
		    while ((text = reader.readLine()) != null) {
				list[i] = text;
		    	i++;
		    }
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    try {
		        if (reader != null) {
		            reader.close();
		        }
		    } catch (IOException e) {
		    }
		}
	}
}

// Simple class that allows the creation of objects with two dimensions, euclidean distance and original position in training set.
// Is used in the sorting of the training instances distance from given test instance yi, while keeping the original index of the training instances
// for classification of test instance yi.
class NearestInstance
{
    public NearestInstance(int position, double distance)
    {
         this.Position = position;
         this.Distance = distance;
    }
    public int Position = 0;
    public double Distance = 0;

    public static PriorityQueue<NearestInstance> sortByDistance(NearestInstance[] instances, int k)
    {
    	PriorityQueue<NearestInstance> queue = new PriorityQueue<NearestInstance>(k, new  DistanceComparator()); //largest element is the root node
    	for (int i = 0; i < instances.length; i++) {
			if (i < k) { // don't have i values in the queue (heap) yet
				queue.add(new NearestInstance(i,instances[i].Distance));
			} else if (instances[i].Distance < queue.element().Distance) { // check if yi instance is smaller than biggest(first) element in the heap (note: constant-time check)
				queue.remove(); //remove biggest (head) in queue (heap)
				queue.add(new NearestInstance(i,instances[i].Distance));
			}
		}
    	return queue;
    }
}

class DistanceComparator implements Comparator<NearestInstance>{
	public int compare(NearestInstance i1, NearestInstance i2) {
		if (i1.Distance > i2.Distance) return -1;
		if (i1.Distance < i2.Distance) return 1;
		return 0;
	}
}